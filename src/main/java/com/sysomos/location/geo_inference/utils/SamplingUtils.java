package com.sysomos.location.geo_inference.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.apache.commons.collections.CollectionUtils;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

public class SamplingUtils {

	public static <T> List<T> reservoir(List<T> elements, int size) {
		if (CollectionUtils.isEmpty(elements) || elements.size() <= size) {
			return elements;
		}
		final Random random = new Random();
		List<T> samples = new ArrayList<T>(size);
		for (int i = 0; i < elements.size(); i++) {
			if (i < size) {
				samples.add(elements.get(i));
			} else {
				int rand = random.nextInt(i);
				if (rand < size) {
					samples.set(rand, elements.get(rand));
				}
			}
		}
		return samples;
	}

	public static <U, V> Multimap<U, V> sample(Multimap<U, V> elements, int size) {
		if (elements == null || elements.size() < size) {
			return elements;
		}
		List<U> userids = new ArrayList<U>(elements.keySet());
		List<U> samples = SamplingUtils.reservoir(userids, size);
		Multimap<U, V> toReturn = ArrayListMultimap.create();
		for (U elem : samples) {
			toReturn.putAll(elem, elements.get(elem));
		}
		return toReturn;
	}
}

