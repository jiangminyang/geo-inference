package com.sysomos.location.geo_inference.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.sysomos.core.search.transfer.Relationship;
import com.sysomos.core.search.twitter.impl.HbaseRelationshipSearchServiceImpl;
import com.sysomos.location.geo_inference.exception.FetchFollowersException;

import scala.Tuple2;

public class FetchFollowers {
	
	private static final Logger LOG = LoggerFactory
			.getLogger(FetchFollowers.class);

	public static Multimap<Long, Long> fetch(Long seed) throws FetchFollowersException {
		return fetch(Arrays.asList(seed));
	}
	
	public static Multimap<Long, Long> fetch(List<Long> seedList) throws FetchFollowersException {

		if (CollectionUtils.isEmpty(seedList)) {
			throw new FetchFollowersException("Seed list is null or empty");
		}
		System.out.println("*******************************");
		System.out.println("FetchFollowers.fetch: start " + new DateTime());

		Multimap<Long, Long> followers = ArrayListMultimap.create();
		try {
			HbaseRelationshipSearchServiceImpl service;
			service = new HbaseRelationshipSearchServiceImpl();
			Map<Long, Collection<Long>> results = service
					.get(Relationship.FOLLOWERS, seedList);

			for (Map.Entry<Long, Collection<Long>> entry : results.entrySet()) {
				followers.putAll(entry.getKey(), entry.getValue());
			}
		} catch (IOException e) {
			LOG.error(e.getLocalizedMessage(), e);
			throw new FetchFollowersException(e);
		}
		System.out.println("FetchFollowers.fetch: end " + new DateTime());
		System.out.println("Follower size: [ " + followers.size() + " ]");
		return followers;
	}
	
	private static List<List<Long>> batches(List<Long> seedList) {
		List<List<Long>> batches = new ArrayList<List<Long>>();
		List<Long> batch = new ArrayList<Long>();
		for(Long id : seedList) {
			if (batch.size() == 50000) {
				batches.add(batch);
				batch = new ArrayList<Long>();
			}
			batch.add(id);
		}
		if (batch.size() != 0) {
			batches.add(batch);
		}
		return batches;
	}
	
	public static JavaPairRDD<Long, Long> fetchIdFollowersPair(List<Long> seedList) {
		JavaRDD<List<Long>> data = spark.getSparkContext().parallelize(batches(seedList));
		JavaPairRDD<Long, Long> result = data.flatMapToPair(new PairFlatMapFunction<List<Long>, Long, Long>() {

			public Iterable<Tuple2<Long, Long>> call(List<Long> batch) throws Exception {
				Multimap<Long, Long> followers = ArrayListMultimap.create();
				try {
					HbaseRelationshipSearchServiceImpl service;
					service = new HbaseRelationshipSearchServiceImpl();
					Map<Long, Collection<Long>> results = service
							.get(Relationship.FOLLOWERS, batch);

					for (Map.Entry<Long, Collection<Long>> entry : results.entrySet()) {
						followers.putAll(entry.getKey(), entry.getValue());
					}
				} catch (IOException e) {
					LOG.error(e.getLocalizedMessage(), e);
					throw new FetchFollowersException(e);
				}
				List<Tuple2<Long, Long>> result = new ArrayList<Tuple2<Long, Long>>();
				for(Long id : followers.keySet()) {
					Collection<Long> follower = followers.get(id);
					for(Long key : follower) {
						result.add(new Tuple2<Long, Long>(id, key));
					}
				}
				return result;
			}
			
		});
		return result;
	}
	
	public static JavaRDD<Long> getFollowersList(JavaPairRDD<Long, Long> data) {
		JavaRDD<Long> rst = data.map(new Function<Tuple2<Long, Long>, Long>() {

			@Override
			public Long call(Tuple2<Long, Long> pair) throws Exception {
				return pair._2;
			}
			
		}).distinct();
		return rst;
	}
	
	public static JavaPairRDD<Long, Iterable<Long>> getFollowerToSeed(JavaPairRDD<Long, Long> data) {
		
		JavaPairRDD<Long, Iterable<Long>> rst = data.mapToPair(new PairFunction<Tuple2<Long, Long>, Long, Long>() {

			@Override
			public Tuple2<Long, Long> call(Tuple2<Long, Long> pair) throws Exception {
				return new Tuple2<Long, Long>(pair._2, pair._1);
			}
			
		}).groupByKey();
		return rst;
	}
	
	public static JavaRDD<Long> fetchFollowersWithLessConfidence(Double confidence) {
		spark.getHiveContext().sql("use lookup_tables");
		DataFrame data;
		if (confidence < 1) {
			data = spark.getHiveContext().sql("select id from user_inferred_geo_table where confidence < " + confidence);
		}
		else {
			data = spark.getHiveContext().sql("select id from user_inferred_geo_table");
		}
		JavaRDD<Long> result = data.javaRDD().map(new Function<Row, Long>() {

			@Override
			public Long call(Row row) throws Exception {
				return row.getLong(0);
			}
			
		}).distinct();
		return result;
	}
}
