package com.sysomos.location.geo_inference.data;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.joda.time.DateTime;

import com.sysomos.activity.service.ActivityService;
import com.sysomos.activity.service.GetActivity;
import com.sysomos.activity.service.UserToUserActivity;
import com.sysomos.activity.service.exception.ActivityServiceException;
import com.sysomos.activity.service.phoenix.LookupUserToFriendsActivityQuery;
import com.sysomos.activity.service.phoenix.LookupUserToUserActivityQuery;
import com.sysomos.core.utils.Pair;

public class UserToFriendsActivities {
	public static Map<Pair<Long, Long>, Integer> getUserToUserActivities(List<Long> idsSetA, List<Long> idsSetB,
			Long startDateMillis, Long endDateMillis) {

		System.out.println("*** Calling ActivityService: start " + new DateTime());

		GetActivity<List<UserToUserActivity>, LookupUserToFriendsActivityQuery>.Batch batch = ActivityService
				.newGetUserToFriendsActivityBatch();
		batch.addRequest(idsSetA, idsSetB, startDateMillis, endDateMillis);
		GetActivity<List<UserToUserActivity>, LookupUserToFriendsActivityQuery>.ResultContext results = batch.call();
		if (results == null || results.size() == 0) {
			String msg = "Results are empty. No activity found between seed " + "user and his/her followers/friends";
			return null;
		}

		Map<Pair<Long, Long>, Integer> activities = new HashMap<Pair<Long, Long>, Integer>();
		for (int i = 0; i < results.size(); i++) {
			try {
				List<UserToUserActivity> activs = results.get(i);
				if (CollectionUtils.isEmpty(activs))
					continue;
				for (UserToUserActivity u2u : activs) {
					if (u2u == null)
						continue;
					Pair<Long, Long> pair = new Pair<Long, Long>(u2u.getIdPair().getA(), u2u.getIdPair().getB());
					if (u2u.getEngagement() == 0) {
						activities.put(pair, 1);
					}
					else {
						activities.put(pair, u2u.getEngagement());
					}
				}
			} catch (ActivityServiceException e) {
			}
		}

		System.out.println("*** Call to ActivityService: end   " + new DateTime());

		return activities;
	}
}