package com.sysomos.location.geo_inference.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;

public class FetchUsersWithAuthorityThreshold {
	protected Connection connection = null;
	protected Statement statement = null;

	public Set<Long> fetch(int threshold) {
		Set<Long> ret = new HashSet<Long>();
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// System.exit(1);
		}
		String user = "labs";
		String pwd = "labs01";
		// 10.13.13.88
		String dbUrl = "jdbc:mysql://10.13.13.88:3306/Lookup_tables?requireSSL=false&useUnicode=true&characterEncoding=UTF-8&jdbcCompliantTruncation=false";

		try {
			connection = DriverManager.getConnection(dbUrl, user, pwd);

			statement = connection.createStatement();
			ResultSet result = statement.executeQuery("SELECT * FROM user_scores_new where authority>=" + threshold);
			while (result.next()) {
				ret.add(result.getLong(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ret;
	}

	public static JavaRDD<Long> fetch(String where_limit_offset, boolean rdd) throws ClassNotFoundException {
		JavaRDD<Long> ret;
		Class.forName("com.mysql.jdbc.Driver");
		String user = "labs";
		String pwd = "labs01";
		// 10.13.13.88
		String dbUrl = "jdbc:mysql://10.13.13.88:3306/Lookup_tables?requireSSL=false&useUnicode=true&characterEncoding=UTF-8&jdbcCompliantTruncation=false";

		Map<String, String> options = new HashMap<String, String>();
		options.put("url",
				"jdbc:mysql://10.13.13.88:3306/Lookup_tables?requireSSL=false&useUnicode=true&characterEncoding=UTF-8&jdbcCompliantTruncation=false&user=labs&password=labs01");
		options.put("dbtable", "(select id from user_scores_new where " + where_limit_offset + ") as authority_table");
		options.put("driver", "com.mysql.jdbc.Driver");
		options.put("numPartitions", "100");
		DataFrame data = spark.getSQLContext().read().format("jdbc").options(options).load();
		ret = data.javaRDD().map(new Function<Row, Long>() {

			@Override
			public Long call(Row row) throws Exception {
				return row.getDecimal(0).longValue();
			}
			
		}).distinct();
		return ret;
	}
}