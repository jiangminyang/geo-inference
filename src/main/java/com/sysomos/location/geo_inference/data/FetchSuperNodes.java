package com.sysomos.location.geo_inference.data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

public class FetchSuperNodes {
	protected Connection connection = null;
	protected Statement statement = null;

	public Set<Long> fetch() {
		Set<Long> ret = new HashSet<Long>();
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// System.exit(1);
		}
		String user = "labs";
		String pwd = "labs01";
		// 10.13.13.88
		String dbUrl = "jdbc:mysql://10.13.13.88:3306/titan?requireSSL=false&useUnicode=true&characterEncoding=UTF-8&jdbcCompliantTruncation=false";

		try {
			connection = DriverManager.getConnection(dbUrl, user, pwd);

			statement = connection.createStatement();
			ResultSet result = statement.executeQuery("SELECT * FROM supernode_twitter");
			while (result.next()) {
				ret.add(result.getLong(1));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ret;
	}
}