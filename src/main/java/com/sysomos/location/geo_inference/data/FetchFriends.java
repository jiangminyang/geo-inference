package com.sysomos.location.geo_inference.data;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.sysomos.core.search.transfer.Relationship;
import com.sysomos.core.search.twitter.impl.HbaseRelationshipSearchServiceImpl;
import com.sysomos.location.geo_inference.exception.FetchFriendsException;

public class FetchFriends {

	private static final Logger LOG = LoggerFactory
			.getLogger(FetchFriends.class);

	public static Multimap<Long, Long> fetch(List<Long> ids, Set<Long> seeds) throws FetchFriendsException {

		if (CollectionUtils.isEmpty(ids)) {
			throw new FetchFriendsException("Seed list is null or empty");
		}
		System.out.println("*******************************");
		System.out.println("FetchFriends.fetch: start " + new DateTime());

		Multimap<Long, Long> friends = ArrayListMultimap.create();
		try {
			HbaseRelationshipSearchServiceImpl service;
			service = new HbaseRelationshipSearchServiceImpl();
			Map<Long, Collection<Long>> results = service
					.get(Relationship.FRIENDS, ids, new HashSet<Long>(seeds));

			for (Map.Entry<Long, Collection<Long>> entry : results.entrySet()) {
				friends.putAll(entry.getKey(), entry.getValue());
			}
		} catch (IOException e) {
			LOG.error(e.getLocalizedMessage(), e);
			throw new FetchFriendsException(e);
		}
		System.out.println("*** Friends size: [ " + friends.size() + " ]");
		System.out.println("FetchFriends.fetch: end " + new DateTime());
		return friends;
	}
}
