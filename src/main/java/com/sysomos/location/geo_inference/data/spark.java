package com.sysomos.location.geo_inference.data;

import java.util.ArrayList;
import java.util.List;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.sql.hive.HiveContext;

import scala.Tuple2;

public class spark {
	static String APP_NAME = "location.geo_inference";
	static private SparkConf sparkConf;
	static private JavaSparkContext sparkContext;
	static private HiveContext hiveContext;
	static private SQLContext sqlContext;
	
	static public void sparkInit(String name) {
		sparkConf = new SparkConf().setAppName(name);
		sparkContext = new JavaSparkContext(sparkConf);
		sparkContext.hadoopConfiguration().set("mapreduce.input.fileinputformat.input.dir.recursive", "true");
		hiveContext = new HiveContext(sparkContext.sc());
		sqlContext = new SQLContext(sparkContext.sc());
		System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "trace");
	}
	
	static public JavaSparkContext getSparkContext() {
		return sparkContext;
	}
	
	static public HiveContext getHiveContext() {
		return hiveContext;
	}

	static public SQLContext getSQLContext() {
		return sqlContext;
	}
	static public void sparkClose() {
		sparkContext.close();
	}

	static public <T> List<JavaRDD<T>> split(JavaRDD<T> data, int num) {
		List<JavaRDD<T>> rst = new ArrayList<JavaRDD<T>>();
		final Broadcast<Integer> broadcastNum = sparkContext.broadcast(num);
		JavaPairRDD<T, Integer> dataPair = data.mapToPair(new PairFunction<T, T, Integer>() {

			@Override
			public Tuple2<T, Integer> call(T id) throws Exception {
				return new Tuple2<T, Integer>(id, (int)(Math.floor((Math.random() * (broadcastNum.value() + 1)))));
			}
			
		});
		for(int i = 0; i < num; i++) {
			final Broadcast<Integer> broadcastInd = sparkContext.broadcast(i);
			rst.add(dataPair.filter(new Function<Tuple2<T, Integer>, Boolean> () {

				@Override
				public Boolean call(Tuple2<T, Integer> id) throws Exception {
					return id._2 == broadcastInd.value();
				}
				
			}).map(new Function<Tuple2<T, Integer>, T>() {

				@Override
				public T call(Tuple2<T, Integer> id) throws Exception {
					return id._1;
				}
				
			}));
		}
		return rst;
	}
	
	static public <K, V> List<JavaPairRDD<K, V>> split(JavaPairRDD<K, V> data, int num) {
		List<JavaPairRDD<K, V>> rst = new ArrayList<JavaPairRDD<K, V>>();
		final Broadcast<Integer> broadcastNum = sparkContext.broadcast(num);
		JavaPairRDD<Tuple2<K, V>, Integer> dataPair = data.mapToPair(new PairFunction<Tuple2<K, V>, Tuple2<K, V>, Integer>() {

			@Override
			public Tuple2<Tuple2<K, V>, Integer> call(Tuple2<K, V> id) throws Exception {
				return new Tuple2<Tuple2<K, V>, Integer>(id, (int)(Math.floor((Math.random() * (broadcastNum.value() + 1)))));
			}
			
		});
		for(int i = 0; i < num; i++) {
			final Broadcast<Integer> broadcastInd = sparkContext.broadcast(i);
			rst.add(dataPair.filter(new Function<Tuple2<Tuple2<K, V>, Integer>, Boolean> () {

				@Override
				public Boolean call(Tuple2<Tuple2<K, V>, Integer> id) throws Exception {
					return id._2 == broadcastInd.value();
				}
				
			}).mapToPair(new PairFunction<Tuple2<Tuple2<K, V>, Integer>, K, V>() {

				@Override
				public Tuple2<K, V> call(Tuple2<Tuple2<K, V>, Integer> id) throws Exception {
					return id._1;
				}
				
			}));
		}
		return rst;
	}
	
}