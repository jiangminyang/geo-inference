package com.sysomos.location.geo_inference.data;

import java.util.ArrayList;
import java.util.List;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;

import scala.Tuple2;

public class FetchSeedsFromHive {
	public static JavaPairRDD<Long, List<Tuple2<String, Double>>> fetch() {
		spark.getHiveContext().sql("USE lookup_tables");
		DataFrame data = spark.getHiveContext().sql("select id, place from seedstable");
		JavaPairRDD<Long, List<Tuple2<String, Double>>> result = data.javaRDD().mapToPair(new PairFunction<Row, Long, List<Tuple2<String, Double>>>() {

			public Tuple2<Long, List<Tuple2<String, Double>>> call(Row row) throws Exception {
				Long id = row.getLong(0);
				List<Row> places = row.getList(1);
				List<Tuple2<String, Double>> placesPair = new ArrayList<Tuple2<String, Double>>();
				for(int i = 0; i < places.size(); i++) {
					placesPair.add(new Tuple2<String, Double>(places.get(i).getString(0), places.get(i).getDouble(1)));
				}
				return new Tuple2<Long, List<Tuple2<String, Double>>>(id, placesPair);
			}
			
		});
	//	sparkContext.close();
		System.out.println(result.count());
		return result;
	}
	public static List<Long> fetchOriginalSeeds() {
		spark.getHiveContext().sql("USE lookup_tables");
		DataFrame data = spark.getHiveContext().sql("select id from seedstable where numofseedfriends=0");
		List<Long> result = data.javaRDD().map(new Function<Row, Long>() {

			public Long call(Row row) throws Exception {
				Long id = row.getLong(0);
				return id;
			}
			
		}).collect();
	//	sparkContext.close();
		System.out.println(result.size());
		return result;
	}
}