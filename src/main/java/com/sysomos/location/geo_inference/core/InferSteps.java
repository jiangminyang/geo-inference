package com.sysomos.location.geo_inference.core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;
import org.apache.spark.broadcast.Broadcast;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sysomos.core.utils.Pair;
import com.sysomos.location.geo_inference.data.FetchFriends;
import com.sysomos.location.geo_inference.data.spark;
import com.sysomos.location.geo_inference.exception.FetchFriendsException;

import scala.Tuple2;

public class InferSteps {
	static final int SIZE_OF_BATCH = 1;
	static final int NUM_OF_BATCHES = 10;
	private static final Logger LOG = LoggerFactory.getLogger(InferSteps.class);

	// private static Map<Long, Iterable<Pair<String, Double>>> seedsTable;
	// public static void setSeedsTable(Map<Long, Iterable<Pair<String,
	// Double>>> table) {
	// seedsTable = table;
	// }
	public static JavaPairRDD<Long, Pair<List<Pair<String, Double>>, Integer>> infer(Collection<Long> list,
			Map<Long, List<Tuple2<String, Double>>> seeds, Long batchSize, int KeepSomeInferredResult) {
		if (KeepSomeInferredResult != 0) {
			return infer(list, seeds, batchSize, KeepSomeInferredResult, 0.8);
		} else {
			return infer(list, seeds, batchSize, 0, 0.0);
		}
	}

	public static JavaPairRDD<Long, Pair<List<Pair<String, Double>>, Integer>> infer(Collection<Long> list,
			Map<Long, List<Tuple2<String, Double>>> seeds, Long batchSize, int KeepSomeInferredResult,
			Double confidence) {
		spark.getHiveContext().sql("use lookup_tables");
		if (KeepSomeInferredResult == 1) {
			DataFrame data = spark.getHiveContext()
					.sql("select * from user_inferred_geo_table where confidence >= " + confidence);
			data = data.repartition(100);
			spark.getHiveContext().sql("drop table user_inferred_geo_table");
			spark.getHiveContext().sql(
					"create table if not exists user_inferred_geo_table (id BIGINT, location STRING, probability Double, confidence Double, place array<struct<location:string, probability:double>>, numOfSeedFriends INT)");
			data.write().insertInto("user_inferred_geo_table");
		} else if (KeepSomeInferredResult == 0) {
			spark.getHiveContext().sql("drop table user_inferred_geo_table");
		}
		List<List<Long>> batches = new ArrayList<List<Long>>();
		List<Long> batch = new ArrayList<Long>();
		for (Long id : list) {
			if (batch.size() == batchSize) {
				batches.add(batch);
				batch = new ArrayList<Long>();
			}
			batch.add(id);
		}
		list = null;
		System.out.println("Number of batches = " + batches.size());
		JavaPairRDD<Long, Pair<List<Pair<String, Double>>, Integer>> inferred = process(batches, seeds);
		return inferred;
	}

	private static JavaPairRDD<Long, Pair<List<Pair<String, Double>>, Integer>> process(List<List<Long>> batches,
			Map<Long, List<Tuple2<String, Double>>> seedsTable) {
		int index = 0;
		JavaPairRDD<Long, Pair<List<Pair<String, Double>>, Integer>> finalResult = null;
		final Broadcast<HashMap<Long, List<Tuple2<String, Double>>>> seeds = spark.getSparkContext()
				.broadcast(new HashMap<Long, List<Tuple2<String, Double>>>(seedsTable));

		while (index < batches.size()) {
			List<List<Long>> smallBatch = new ArrayList<List<Long>>();
			while (index < batches.size() && smallBatch.size() < SIZE_OF_BATCH) {
				smallBatch.add(batches.get(index));
				index++;
			}
			System.out.println("start " + index / SIZE_OF_BATCH + "th batch at " + new Date().toString());
			JavaRDD<List<Long>> data = spark.getSparkContext().parallelize(smallBatch);

			JavaPairRDD<Long, Pair<List<Pair<String, Double>>, Integer>> result = data.flatMapToPair(
					new PairFlatMapFunction<List<Long>, Long, Pair<List<Pair<String, Double>>, Integer>>() {

						@Override
						public Iterable<Tuple2<Long, Pair<List<Pair<String, Double>>, Integer>>> call(List<Long> batch)
								throws Exception {
							List<Tuple2<Long, Pair<List<Pair<String, Double>>, Integer>>> result = new ArrayList<Tuple2<Long, Pair<List<Pair<String, Double>>, Integer>>>();
							List<Long> followers = batch;
							Map<Long, Collection<Long>> friends = new HashMap<Long, Collection<Long>>();
							try {
								friends = FetchFriends.fetch(followers, seeds.value().keySet()).asMap();
							} catch (FetchFriendsException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							Set<Long> seedsSet = new HashSet<Long>(seeds.value().keySet());
							for (Long id : followers) {
								Collection<Long> friendsOfOneFollower = friends.get(id);
								Map<String, Double> locationScoreMap = new HashMap<String, Double>();
								Integer numOfSeedFriend = 0;
								if (friendsOfOneFollower == null)
									continue;
								for (Long key : friendsOfOneFollower) {
									if (seedsSet.contains(key)) {
										numOfSeedFriend += 1;
										Iterable<Tuple2<String, Double>> places = seeds.value().get(key);
										for (Tuple2<String, Double> pair : places) {
											if (locationScoreMap.containsKey(pair._1)) {
												locationScoreMap.put(pair._1, locationScoreMap.get(pair._1) + pair._2);
											} else {
												locationScoreMap.put(pair._1, pair._2);
											}
										}
									}
								}
								List<Pair<String, Double>> resultList = new ArrayList<Pair<String, Double>>();
								for (String loc : locationScoreMap.keySet()) {
									resultList.add(new Pair<String, Double>(loc,
											locationScoreMap.get(loc) / (numOfSeedFriend * 1.0)));
								}
								result.add(new Tuple2<Long, Pair<List<Pair<String, Double>>, Integer>>(id,
										new Pair<List<Pair<String, Double>>, Integer>(resultList, numOfSeedFriend)));
							}
							return result;
						}
					});
			// saveToHive(result);
			System.out.println("finish " + index / SIZE_OF_BATCH + "th batch at " + new Date().toString());
		}
		System.out.println("finish processing");
		return null;
	}

	public static void saveToHive(JavaPairRDD<Long, Pair<List<Tuple2<String, Double>>, Integer>> data) {
		System.out.println("start writing to hive at " + (new Date()).toString());
		spark.getHiveContext().sql("use lookup_tables");
		// hiveContext.sql("drop table user_inferred_geo_table");
		spark.getHiveContext().sql(
				"create table if not exists user_inferred_geo_table (id BIGINT, location String, probability Double, confidence DOUBLE, top_locations array<struct<location:string, probability:double>>, place array<struct<location:string, probability:double>>, numOfSeedFriends INT)");
		List<StructField> fields = new ArrayList<StructField>();
		List<StructField> loc = new ArrayList<StructField>();
		loc.add(DataTypes.createStructField("key", DataTypes.StringType, false));
		loc.add(DataTypes.createStructField("value", DataTypes.DoubleType, false));

		fields.add(DataTypes.createStructField("id", DataTypes.LongType, false));
		fields.add(DataTypes.createStructField("location", DataTypes.StringType, false));
		fields.add(DataTypes.createStructField("probability", DataTypes.DoubleType, false));
		fields.add(DataTypes.createStructField("confidence", DataTypes.DoubleType, false));
		fields.add(DataTypes.createStructField("top_locations",
				DataTypes.createArrayType(DataTypes.createStructType(loc)), true));
		fields.add(DataTypes.createStructField("place", DataTypes.createArrayType(DataTypes.createStructType(loc)),
				false));
		fields.add(DataTypes.createStructField("numOfSeedFriends", DataTypes.IntegerType, false));
		StructType tableSchema = DataTypes.createStructType(fields);
		JavaRDD<Row> followerGeoTable = data
				.map(new Function<Tuple2<Long, Pair<List<Tuple2<String, Double>>, Integer>>, Row>() {

					public Row call(Tuple2<Long, Pair<List<Tuple2<String, Double>>, Integer>> row) throws Exception {
						Double max = 0.0;
						Double secondMax = 0.0;
						String loc = "";
						List<Tuple2<String, Double>> places = row._2.getA();
						Collections.sort(places, new Comparator<Tuple2<String, Double>>() {

							@Override
							public int compare(Tuple2<String, Double> a, Tuple2<String, Double> b) {
								if (a._2 > b._2)
									return -1;
								if (a._2 < b._2)
									return 1;
								return 0;
							}

						});
						List<Row> top_loc = new ArrayList<Row>();
						List<Row> place = new ArrayList<Row>();
						boolean addToTopLoc = true;
						for (int i = 0; i < places.size(); i++) {
							if (places.get(i)._2 > max) {
								secondMax = max;
								max = places.get(i)._2;
								loc = places.get(i)._1;
							} else {
								if (places.get(i)._2 > secondMax) {
									secondMax = places.get(i)._2;
								}
							}
							if (addToTopLoc) {
								top_loc.add(RowFactory.create(places.get(i)._1, places.get(i)._2));
								if (i == places.size() - 1 || places.get(i)._2 > 2 * places.get(i + 1)._2) {
									addToTopLoc = false;
								}
							}
							place.add(RowFactory.create(places.get(i)._1, places.get(i)._2));
						}
						Double confidence = 0.0;
						if (max > 1e-10) {
							confidence = (max - secondMax) / max;
						}
						return RowFactory.create(row._1, loc, max, confidence, top_loc, place, row._2.getB());
					}

				});
		DataFrame df = spark.getHiveContext().createDataFrame(followerGeoTable, tableSchema);
		df.write().insertInto("user_inferred_geo_table");
		// sparkContext.close();
		System.out.println("finish writing to hive at " + (new Date()).toString());
	}

	public static Double magicFunc(Double num, int n) {
		if (num <= 0.5) {
			return ((Math.exp(n * num) - 1) / (2 * (Math.exp(n / 2.0) - 1)));
		}
		return ((Math.log(2 * (Math.exp(n / 2.0) - 1) * (num - 0.5) + 1) / (n * 1.0) + 5));
	}

	public static JavaPairRDD<Long, Pair<List<Tuple2<String, Double>>, Integer>> infer(
			Map<Long, List<Tuple2<String, Double>>> seedsTable, JavaPairRDD<Long, Iterable<Long>> followerToSeeds) {
		spark.getHiveContext().sql("use lookup_tables");
		spark.getHiveContext().sql("drop table user_inferred_geo_table");
		final Broadcast<HashMap<Long, List<Tuple2<String, Double>>>> seeds = spark.getSparkContext()
				.broadcast(new HashMap<Long, List<Tuple2<String, Double>>>(seedsTable));
		int counter = 0;
		JavaPairRDD<Long, Pair<List<Tuple2<String, Double>>, Integer>> rst = followerToSeeds
				.filter(new Function<Tuple2<Long, Iterable<Long>>, Boolean>() {

					@Override
					public Boolean call(Tuple2<Long, Iterable<Long>> follower) throws Exception {
						return !(seeds.value().containsKey(follower._1));
					}

				}).mapToPair(
						new PairFunction<Tuple2<Long, Iterable<Long>>, Long, Pair<List<Tuple2<String, Double>>, Integer>>() {

							@Override
							public Tuple2<Long, Pair<List<Tuple2<String, Double>>, Integer>> call(
									Tuple2<Long, Iterable<Long>> user) throws Exception {
								Iterable<Long> friendsOfOneFollower = user._2;
								Map<String, Tuple2<Double, Integer>> locationScoreMap = new HashMap<String, Tuple2<Double, Integer>>();

								Integer numOfSeedFriend = 0;
								// for (Long key : friendsOfOneFollower) {
								// numOfSeedFriend += 1;
								// Iterable<Tuple2<String, Double>> places =
								// seeds.value().get(key);
								// for (Tuple2<String, Double> pair : places) {
								// if (locationScoreMap.containsKey(pair._1)) {
								// List<Double> temp =
								// locationScoreMap.get(pair._1);
								// temp.add(pair._2);
								// locationScoreMap.put(pair._1, temp);
								// } else {
								// List<Double> temp = new ArrayList<Double>();
								// temp.add(pair._2);
								// locationScoreMap.put(pair._1, temp);
								// }
								// }
								// }
								// List<Tuple2<String, Double>> list = new
								// ArrayList<Tuple2<String, Double>>();
								// Double total = 0.0;
								// for (String loc : locationScoreMap.keySet())
								// {
								// List<Double> temp =
								// locationScoreMap.get(loc);
								// int n = temp.size();
								// Double sum = 0.0;
								// for (Double num : temp) {
								// sum += magicFunc(num, n);
								// }
								// sum /= n;
								// total += sum;
								// list.add(new Tuple2<String, Double>(loc,
								// sum));
								// }
								// List<Tuple2<String, Double>> resultList = new
								// ArrayList<Tuple2<String, Double>>();
								// for (Tuple2<String, Double> loc : list) {
								// resultList.add(new Tuple2<String,
								// Double>(loc._1, loc._2 / total));
								// }
								// list = null;
								for (Long key : friendsOfOneFollower) {

									numOfSeedFriend += 1;
									Iterable<Tuple2<String, Double>> places = seeds.value().get(key);
									for (Tuple2<String, Double> pair : places) {
										if (locationScoreMap.containsKey(pair._1)) {
											Tuple2<Double, Integer> temp = locationScoreMap.get(pair._1);
											locationScoreMap.put(pair._1,
													new Tuple2<Double, Integer>(temp._1 + pair._2, temp._2 + 1));
										} else {
											locationScoreMap.put(pair._1, new Tuple2<Double, Integer>(pair._2, 1));
										}
									}

								}
								List<Tuple2<String, Double>> resultList = new ArrayList<Tuple2<String, Double>>();
								Double total = 0.0;
								for (String loc : locationScoreMap.keySet()) {
									Tuple2<Double, Integer> temp = locationScoreMap.get(loc);
									Double value = temp._2 > 1 ? magicFunc(temp._1 / (numOfSeedFriend * 1.0), temp._2)
											: temp._1 / (numOfSeedFriend * 1.0);
									total += value;
									resultList.add(new Tuple2<String, Double>(loc, value));
								}
								for (int i = 0; i < resultList.size(); i++) {
									Tuple2<String, Double> temp = resultList.get(i);
									resultList.set(i, new Tuple2<String, Double>(temp._1, temp._2 / total));
								}
								return (new Tuple2<Long, Pair<List<Tuple2<String, Double>>, Integer>>(user._1,
										new Pair<List<Tuple2<String, Double>>, Integer>(resultList, numOfSeedFriend)));
							}

						});
		return rst;
	}

	public static JavaPairRDD<Long, List<Tuple2<String, Double>>> seedExpansion(
			JavaPairRDD<Long, Pair<List<Tuple2<String, Double>>, Integer>> data, Double probThreshold,
			int seedFriendThreshold) {
		final Broadcast<Double> probBroadcast = spark.getSparkContext().broadcast(probThreshold);
		final Broadcast<Integer> seedFriendBroadcast = spark.getSparkContext().broadcast(seedFriendThreshold);
		JavaPairRDD<Long, List<Tuple2<String, Double>>> rst = data
				.filter(new Function<Tuple2<Long, Pair<List<Tuple2<String, Double>>, Integer>>, Boolean>() {

					@Override
					public Boolean call(Tuple2<Long, Pair<List<Tuple2<String, Double>>, Integer>> user)
							throws Exception {
						Double prob = probBroadcast.value();
						Integer seedFriend = seedFriendBroadcast.value();
						if (user._2.getB() < seedFriend) {
							return false;
						}
						Double max = 0.0;
						for (Tuple2<String, Double> id : user._2.getA()) {
							if (id._2 > max)
								max = id._2;
						}
						if (max <= prob) {
							return false;
						}
						return true;
					}

				}).mapToPair(
						new PairFunction<Tuple2<Long, Pair<List<Tuple2<String, Double>>, Integer>>, Long, List<Tuple2<String, Double>>>() {

							@Override
							public Tuple2<Long, List<Tuple2<String, Double>>> call(
									Tuple2<Long, Pair<List<Tuple2<String, Double>>, Integer>> user) throws Exception {
								return new Tuple2<Long, List<Tuple2<String, Double>>>(user._1, user._2.getA());
							}

						});
		return rst;
	}
}