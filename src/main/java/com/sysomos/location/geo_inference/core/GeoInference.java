package com.sysomos.location.geo_inference.core;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.sysomos.core.utils.Pair;
import com.sysomos.location.geo_inference.data.FetchFollowers;
import com.sysomos.location.geo_inference.data.FetchSeedsFromHive;
import com.sysomos.location.geo_inference.data.FetchSuperNodes;
import com.sysomos.location.geo_inference.data.FetchUsersWithAuthorityThreshold;
import com.sysomos.location.geo_inference.data.spark;
import com.sysomos.location.geo_inference.exception.FetchFollowersException;
import com.sysomos.location.geo_inference.exception.FetchFriendsException;

import scala.Tuple2;

public class GeoInference {
	private static final int THRESHOLD = 10;

	public static class Parameters {
		@Parameter(names = { "-batch", "-b" }, required = false, description = "batch size")
		private Long batch = 100000L;
		@Parameter(names = { "-m", "-mode" }, required = false, description = "run mode, 0 means the run before expansion, 1 means after expansion and rerun for previous followers + some high authority people, 1 means only for new people with certain authority")
		private Integer mode = 0;
		@Parameter(names = { "-a", "-authority" }, required = false, description = "authority score threshold")
		private Integer authority = 5;
		@Parameter(names = { "-c",
				"-confidence" }, required = false, description = "redo inferred for people whose confidence score less than threshold")
		private Double confidence = 1.1;
		@Parameter(names = { "-f",
				"-seedfriends" }, required = false, description = "redo inferred for people whose seed friend smaller than this number")
		private Integer seedfriends = 3;
		@Parameter(names = { "-l", "-limit" }, required = false, description = "limit for ")
		private Long limit = 20000000L;
		@Parameter(names = { "-o",
				"-offset" }, required = false, description = "redo inferred for people whose seed friend smaller than this number")
		private Long offset = 0L;
		@Parameter(names = { "-p"}, required = false, description = "seed expansion threshold for location probability")
		private Double probThreshold = 0.75;
		@Parameter(names = { "-s"}, required = false, description = "seed expansion threshold for location num of seedsfriend")
		private Integer seedsFriendThreshold = 5;
		@Parameter(names = { "-p2"}, required = false, description = "seed expansion threshold for location probability")
		private Double probThreshold2 = 0.85;
		@Parameter(names = { "-s2"}, required = false, description = "seed expansion threshold for location num of seedsfriend")
		private Integer seedsFriendThreshold2 = 5;
	}

	public static void main(String args[])
			throws FetchFollowersException, FetchFriendsException, ClassNotFoundException {
		Parameters params = new Parameters();
		new JCommander(params, args).parse();
		spark.sparkInit("geo_inference");
		long time = (new Date()).getTime();
		JavaPairRDD<Long, List<Tuple2<String, Double>>> seedsRDDWithPlaces = FetchSeedsFromHive.fetch();
		Map<Long, List<Tuple2<String, Double>>> seeds = seedsRDDWithPlaces.collectAsMap();
		List<Long> seedsList = new ArrayList<Long>();
		Set<Long> seedsSet = new HashSet<Long>();
		int count = 0;
		for (Long id : seeds.keySet()) {
			seedsSet.add(id);
			// if (count++ > 10000) break;
		}
		seedsSet.removeAll((new FetchSuperNodes()).fetch());
		seedsList.addAll(seedsSet);
		JavaRDD<Long> seedRDD = spark.getSparkContext().parallelize(seedsList);

		if (params.mode == 0) { // first run before expansion
			JavaPairRDD<Long, Long> seedToFollower = FetchFollowers.fetchIdFollowersPair(seedsList);
			JavaPairRDD<Long, Iterable<Long>> FollowerToSeeds = FetchFollowers.getFollowerToSeed(seedToFollower);
			System.out.println("follower size = " + FollowerToSeeds.count());
			JavaPairRDD<Long, Pair<List<Tuple2<String, Double>>, Integer>> firstRunResult = InferSteps.infer(seeds, FollowerToSeeds);
			
	
			//second run
			seedsRDDWithPlaces = seedsRDDWithPlaces.union(InferSteps.seedExpansion(firstRunResult, params.probThreshold, params.seedsFriendThreshold));
			seeds = null; seedsList = null; seedsSet = null;
			seeds = seedsRDDWithPlaces.collectAsMap();
			System.out.println("expanded seed size = " + seeds.size());
			seedsList = new ArrayList<Long>();
			seedsSet = new HashSet<Long>();
			for (Long id : seeds.keySet()) {
				seedsSet.add(id);
			}
			seedsSet.removeAll((new FetchSuperNodes()).fetch());
			seedsList.addAll(seedsSet);
			seedRDD = spark.getSparkContext().parallelize(seedsList);
			seedToFollower = null;
			FollowerToSeeds = null;
			
			seedToFollower = FetchFollowers.fetchIdFollowersPair(seedsList);
			FollowerToSeeds = (FetchFollowers.getFollowerToSeed(seedToFollower));
			System.out.println("follower size = " + FollowerToSeeds.count());
			seeds = seedsRDDWithPlaces.collectAsMap();
			JavaPairRDD<Long, Pair<List<Tuple2<String, Double>>, Integer>> secondRunResult = InferSteps.infer(seeds, FollowerToSeeds);
//			
//			//third run
//			seedsRDDWithPlaces = seedsRDDWithPlaces.union(InferSteps.seedExpansion(secondRunResult, params.probThreshold2, params.seedsFriendThreshold2));
//			seeds = null; seedsList = null; seedsSet = null;
//			seeds = seedsRDDWithPlaces.distinct().collectAsMap();
//			System.out.println("expanded seed size = " + seeds.size());
//			seedsList = new ArrayList<Long>();
//			seedsSet = new HashSet<Long>();
//			for (Long id : seeds.keySet()) {
//				seedsSet.add(id);
//			}
//			seedsSet.removeAll((new FetchSuperNodes()).fetch());
//			seedsList.addAll(seedsSet);
//			seedRDD = spark.getSparkContext().parallelize(seedsList);
//			seedToFollower = null;
//			FollowerToSeeds = null;
//			
//			seedToFollower = FetchFollowers.fetchIdFollowersPair(seedsList);
//			FollowerToSeeds = FetchFollowers.getFollowerToSeed(seedToFollower);
//			System.out.println("follower size = " + FollowerToSeeds.count());
//			JavaPairRDD<Long, Pair<List<Tuple2<String, Double>>, Integer>> thirdRunResult = InferSteps.infer(seeds, FollowerToSeeds);
//			
			
			InferSteps.saveToHive(secondRunResult);
//		} else if (params.mode == 1) { // after expansion 
//			JavaRDD<Long> followersData = FetchFollowers.fetchFollowersWithLessConfidence(params.confidence);
//			JavaRDD<Long> unknownSet = followersData;
//			JavaRDD<Long> userWithHighAuthorityScore = FetchUsersWithAuthorityThreshold
//					.fetch("authority >= " + params.authority, true);
//			unknownSet = unknownSet.union(userWithHighAuthorityScore);
//			userWithHighAuthorityScore = null;
//			unknownSet = unknownSet.subtract(spark.getSparkContext().parallelize(seedsList));
//			time = (new Date()).getTime();
//			System.out.println(unknownSet.count());
//			InferSteps.infer(unknownSet.collect(), seeds, params.batch, 0);
		} else { // after expansion infer for people who has authority >=
					// threshold but not inferred before
			JavaRDD<Long> followersData = FetchFollowers.fetchFollowersWithLessConfidence(params.confidence);
			JavaRDD<Long> knownSet = followersData;
			knownSet = knownSet.union(spark.getSparkContext().parallelize(seedsList));
			JavaRDD<Long> userWithAuthorityScore = FetchUsersWithAuthorityThreshold.fetch(
					"authority = " + params.authority + " limit " + params.limit + " offset " + params.offset, true);
			JavaRDD<Long> unknownSet = userWithAuthorityScore;
			System.out.println("known data size = " + knownSet.count());
			System.out.println("people in list : " + unknownSet.count());
			unknownSet = unknownSet.subtract(knownSet);
			time = (new Date()).getTime();
			System.out.println("unknownSet Size : " + unknownSet.count());
			List<Long> unknownList = unknownSet.collect();
			unknownSet.unpersist();
			knownSet.unpersist();
			unknownSet = knownSet = null;
			InferSteps.infer(unknownList, seeds, params.batch, 2, 1.1);
		}
		// InferSteps.saveToHive(inferred);
		System.out.println("time used = " + ((new Date().getTime() - time) / 1000 / 60) + " minutes.");
		spark.sparkClose();
	}
}