package com.sysomos.location.geo_inference.exception;

public class GeoInferenceException extends Exception {

	private static final long serialVersionUID = -6611428769319094677L;

	public GeoInferenceException(String message) {
		super(message);
	}

	public GeoInferenceException(Throwable cause) {
		super(cause);
	}

	public GeoInferenceException(Exception e) {
		super(e);
	}
}
